#!/usr/bin/octave



function writeToFile (u,v,file)

  fid=fopen(file,'w')

  for i = 1:(length(u)-1)
    printf ('%d %e\n', u(:,i), v(:,i));
    fprintf (fid,'%d %e\n', u(:,i), v(:,i));
  endfor

  fclose(fid);
endfunction

x = 0:50
b = binopdf(x,50,0.3)
p = poisspdf(x,15)
g = normpdf(x, 15, sqrt(10.5))

writeToFile (x,b,'/tmp/binomial');
writeToFile (x,p,'/tmp/poisson');
writeToFile (x,g,'/tmp/normal');
